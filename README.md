# dl-ansible-and-molecule

Builds a docker image which uses `docker` inside an Ubuntu base image and has the latest stable versions of Ansible and Molecule installed via pip.

## Purpose

This docker container can be used in a CI/CD pipeline in place of the `quay.io/ansible/molecule:latest` image which ships with an older version of Ansible (from Alpine's system packages), and a version of Molecule built from source. This image builds Ansible and Molecule from the source code on PyPi, providing the user with _stable_ versions of both pieces of software, similar to what they would use for molecule testing on their local machine.

## Usage

This container provides both the `ansible` and `molecule` commands, so run tests inside the `script:` block of your `.gitlab-ci.yml` file just like you would if testing with `molecule` on your local machine.

## Distribution

The image is based off of the Ubuntu base image.

## License

See LICENSE file for details.
